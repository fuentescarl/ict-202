<?php
	include 'header.php';
?>
<?php require('../controllers/MysqlConnect.php');
$conn=myConnect();
?>
<?php
if(isset($_POST['submit'])){
}else


$sql = "SELECT  photo, productName, productDescription, isAvailable, buyPrice, productID FROM products";
$result = mysqli_query($conn,$sql)
or 
die("Error: ".mysqli_error($conn));

?>
<style>
/* Style the Image Used to Trigger the Modal */
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption { 
    animation-name: zoom;
    animation-duration: 0.6s;
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>
    <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">What do we
                        <strong>Serve?</strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-md-6">
                    <img class="img-responsive img-border-left" src="../img/Cakes/header/black-forest-cake-A.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <p></p>
                    <p>We serve freshly baked cakes at a very Affordable Price</p>
					<br></br>
					<br></br>
					<br></br>
					
				
					<h1> Check Out Our Menu Below</h1>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                  
                    <h2 class="intro-text text-center">Starlight
                       <strong>Bakeshop</strong> 	
			<a href="viewCart.php">
			<button class='btn btn-lg btn-success pull-right'>Cart <span class="badge"><?php?></span></button>
					   <hr>
					</h2>
		
					</a>
                    </h2>

					<div class='panel panel-primary' style='width:100%; margin:5% auto;'>
	<div class='panel-heading'>
	<h1 style='text-align:center;'>Cakes Menu</h1>
	</div>
	<div class='panel-body'>
	<table id='productTable'>
		<thead>
			<th>Photo</th>
			<th>Product Name</th>
			<th>Description</th>
			<th>Availability</th>
			<th>Price</th>
			<th>Actions</th>
		</thead>
		
	<tbody>
	<?php
		while($row = mysqli_fetch_array($result)){ ?>
		<tr>

		<td><img id="myImg" src="../images/<?php  echo $row[0];?>" height="100" width="150"> </td>
		<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>
		<td><?php  echo $row[1]; ?> </td>
		<td><?php  echo $row[2]; ?> </td>
		<td><?php  echo $row[3]; ?> </td>
		<td><?php  echo $row[4]; ?> </td>
		<td>
		<?php

		echo "<div class='text-center'>
				<a href='addCart.php?pCode={$row['productID']}'>
					<button value='submit'	name='submit' class='btn btn-success'>ADD TO CART</button>
				</a>
			</div>";
		echo "</div>";
	
	?>
	<?php } ?>
	</tbody>
	</table>
	</div>
                </div>
                
			     <div class="col-sm-4 text-center">
				<!-- wewewewewewew -->
				
			
		<!-- wewewewewewew -->	
			

        </div>

    </div>
    <!-- /.container -->
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<?php
	include 'footer.php';
?>