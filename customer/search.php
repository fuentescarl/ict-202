<?php
include 'header.php';
?>
<?php require('../controllers/MysqlConnect.php');
$conn=myConnect();
?>



 <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Search
                        <strong>Orders</strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-lg-12 text-center">
                    <form action="" method="POST" enctype="multipart/form-data">
				<div class="form-group">
				<label for="exampleInputEmail1" >Search Orders</label>
				<input type="text" class="form-control" name="searchName" id="exampleInputEmail1" placeholder="Search Orders">
			  </div>
			  <button  id="show_button" onclick="document.getElementById('id01').style.display='block'" style="width:auto" type="submit" name="submit" value="submit" class="btn btn-success">
			 Search
			  </button>
					</form>

                </div>
            </div>
        </div>
    </div>

<?php

if(isset($_POST['submit'])){
	$searchName = $_POST['searchName'];
	$sql="SELECT orderID, customer.customerFirstName, customer.customerLastName, orders.orderStatus, orders.customerID
			FROM customer
			JOIN orders
			ON orders.orderID=customer.customerID
			WHERE customer.customerID LIKE '%$searchName%' OR customer.customerFirstName LIKE '%$searchName%' ";
	$result=$conn->query($sql);
	while($row=$result->fetch_assoc()){
	echo "
	 <div class='panel-body'>
            <div class='box'>
	 
		<center>
		<table id='productTable'>
		<thead>
			<th>Orders</th>
			<th>Customer Name</th>
			<th>Order Status</th>
		</thead>
		<tbody>	
	
		<td><a href='customerOrders.php?id={$row['orderID']}'><button type='button' class='btn btn-success'>View Orders</button></a></td>
		
		<td>".$row['customerFirstName']." ".$row['customerLastName']."</td>
		<td>".$row['orderStatus']."</td>		
				 </tbody>
				</table>
				 </center>
				  </div>
				  </div>
					
				  ";
				  
		}       
}else{
	echo '<center><div class="alert alert-success" role="alert" style="width: 85%;">'."Search Name"."</div></center>";
}
?>

<script src='../datatable_files/jquery.min.js'></script>
<script src='../datatable_files/datatables.min.js'></script>
<script>
$(document).ready(function(){
	$("#productTable").DataTable();
	});
</script>
<?php
include "footer.php";
?>