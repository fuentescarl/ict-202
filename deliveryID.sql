-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2018 at 07:08 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my136proj`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `uname` varchar(32) DEFAULT NULL,
  `pword` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`uname`, `pword`) VALUES
('admin', '123qwe');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customerID` int(32) NOT NULL,
  `customerFirstName` varchar(32) NOT NULL,
  `customerLastName` varchar(32) NOT NULL,
  `customerAdd` varchar(32) NOT NULL,
  `customerContact` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderID` int(12) NOT NULL,
  `customerID` int(32) NOT NULL,
  `customerName` varchar(32) NOT NULL,
  `orderStatus` varchar(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `orderDetails` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `order_contents` text NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `photo` varchar(32) NOT NULL,
  `productID` int(15) NOT NULL,
  `productName` varchar(70) NOT NULL,
  `productDescription` text NOT NULL,
  `isAvailable` varchar(6) NOT NULL,
  `buyPrice` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`photo`, `productID`, `productName`, `productDescription`, `isAvailable`, `buyPrice`) VALUES
('1521274154appcake.jpg', 2, 'Apple Cake', 'Apple, caramel icing', 'Yes', '70.00'),
('1521274220avoc_cake.jpg', 3, 'Avocado Cake', 'Prepared by using avocado as a primary ingredient', 'Yes', '60.00'),
('1521274244bananacake.jpg', 4, 'Banana Cake', 'Prepared by using banana as a primary ingredient', 'Yes', '60.00'),
('1521274266butterc.jpg', 5, 'Butter Cake', 'Buttered Cake', 'Yes', '65.00'),
('1521274294cheesec.jpg', 6, 'Cheese Cake', 'Thin base made from crushed biscuits, with a thicker top layer of soft cheese, eggs and sugar. It can be baked or unbaked', 'Yes', '70.00'),
('1521274317chiffon.jpg', 7, 'Chiffon Cake', 'Light, airy cake made with vegetable oil, eggs, sugar, flour', 'Yes', '50.00'),
('1521274369chococake.jpg', 8, 'Chocolate Cake', 'Chocolate Coated Cake', 'Yes', '80.00'),
('1521274411Chocolatechestnut.jpg', 9, 'Chestnut Cake', 'Prepared using chestnuts or water chestnuts as a main ingredient. It is a dish in Chinese cuisine', 'Yes', '80.00'),
('1521274436mousse.jpg', 10, 'Chocolate Mousse Cake', 'A non-baked cake dessert made by mixing broken Marie biscuits, combined with a chocolate sauce or runny custard', 'Yes', '85.00'),
('1521636413Hydrangeas.jpg', 12, 'Hmm Cake', 'Testing Cakeyy Hmm', 'Yes', '30.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderID`),
  ADD UNIQUE KEY `customerID` (`customerID`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`orderDetails`),
  ADD KEY `orderID_2` (`orderID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD UNIQUE KEY `productCode` (`productID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customerID` int(32) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderID` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `orderDetails` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productID` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customerID`) REFERENCES `customer` (`customerID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
