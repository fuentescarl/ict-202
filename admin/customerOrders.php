<?php include "header.php"; ?>
<?php require('../controllers/MysqlConnect.php'); 
$conn=myConnect(); ?>
<?php
$sql = "SELECT customerName, customer.customerLastName, orderStatus
		FROM orders 
		JOIN customer
		ON orders.orderID=customer.customerID
		WHERE orderID = {$_GET['id']}";
$result	= mysqli_query($conn,$sql);
$data = mysqli_fetch_assoc($result);
$space=" ";
$priceis="Price is: ";
$sql2 = "SELECT order_contents,price, orderID
		FROM order_details
		WHERE orderID = {$_GET['id']}";
$result2	= mysqli_query($conn,$sql2);

?>

<!-- start of content -->
	<div class="container" style="background-color:white; padding:150px; border-radius:10px; margin-bottom: 50px;">
	
	
	<div class="row">
		<div class="col-md-6">
			<form action="" method="POST" enctype="multipart/form-data">
			
			<div class="form-group">
			<label for="exampleInputEmail1" >Customer Name</label>
				<p><?php echo $data['customerName'],$space,$data['customerLastName']; ?></p>
			 </div>
			 
			 
			 <div class="form-group">
			<label for="exampleInputEmail1" >Customer Orders</label>
			<table>
			<thead>
			<th>Product Name</th>
			<th></th>
			<th>Price</th>
			</thead>
		<tbody>
				<?php 
				while ($data2 = mysqli_fetch_assoc($result2)){ ?>
				<tr>
					<td><?php  echo $data2['order_contents']; ?> </td>
					<td><?php echo $data2['price']; ?> </td>
				<?php } ?>
					</tbody>
				</table>
			 </div>
			 
			<div class="form-group">
			<label for="exampleInputEmail1" >Order Status</label>
			<p><?php echo "$data[orderStatus]"; ?></p>
			</div>
		
				
		
			  <button class="btn btn-danger"><a href="status.php">Back</a></button>
			</form>
				
		</div>
		<div class="col-md-6">
		</div>
	</div>
		
	</div>

<!-- end of content -->
<?php
	include 'footer.php';
?>