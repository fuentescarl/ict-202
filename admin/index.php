<?php
	include 'header.php';
?>
    <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12 text-center">
                    <div id="carousel-example-generic" class="carousel slide">
                        <!-- Indicators -->
                        <ol class="carousel-indicators hidden-xs">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="3" class="active"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div>
                                <img src="../img/Cakes/header/black-forest-cake-A.jpg" alt="">
                            </div>
                        </div>


                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="icon-prev"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="icon-next"></span>
                        </a>
                    </div>
                    <h2 class="brand-before">
                        <small>Welcome to</small>
                    </h2>
                    <h1 class="brand-name">Criskatt Cakeshop</h1>
                    <hr class="tagline-divider">
                    <h2>
                    </h2>
                </div>
            </div>
        </div>   

    </div>
 
 <?php
	include 'footer.php';
?>
