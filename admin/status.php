<?php
include 'header.php';
?>
<?php require('../controllers/MysqlConnect.php');
$conn=myConnect();
?>
<?php
$sql = "SELECT customer.customerFirstName, customer.customerLastName, customer.customerContact ,orders.orderStatus 
			,orders.orderID
			FROM customer
			JOIN orders
			ON orders.orderID=customer.customerID";
$result = mysqli_query($conn,$sql)
or 
die("Error: ".mysqli_error($conn));


?>

<html>
<body>
	<div class='panel panel-primary' style='width:90%; margin:5% auto;'>
	<div class='panel-heading'>
	<h1 style='text-align:center;'>Edit Order Status</h1>
	</div>
	<div class='panel-body'>
	<table id='productTable'>
		<thead>
			<th>Actions</th>
			<th></th>
			<th></th>
			<th>Customer Name</th>
			<th>Customer Contact</th>
			<th>Customer ID</th>
			<th>Order Status</th>
			
			</thead>
		</thead>

	<tbody>
	<?php
	
		while($row = mysqli_fetch_array($result)){ 
		echo "<tr>";
		echo "<td><a href='customerEdit.php?id={$row['orderID']}'><button type='button' class='btn btn-success'>Edit Order Status</button></a></td>";
		echo "<td><a href='customerOrders.php?id={$row['orderID']}'><button type='button' class='btn btn-success'>View Orders</button></a></td>";
		echo "<td><a href='deleteOrder.php?id={$row['orderID']}'><button type='button' class='btn btn-danger'>Delete</button></a></td>";
		echo "<td> $row[0] $row[1]</td>";
		echo "<td> $row[2]</td>";
		echo "<td> $row[4]</td>";
		echo "<td> $row[3]</td>";
		
		echo "</tr>";
	 } 
	 ?>



	
	</tbody>
	</table>
	</div>
</body>
</html>
<script src='../datatable_files/jquery.min.js'></script>
<script src='../datatable_files/datatables.min.js'></script>
<script>
$(document).ready(function(){
	$("#productTable").DataTable();
	});
</script>

<?php
include 'footer.php';
?>
