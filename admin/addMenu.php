<?php
	
	include 'header.php';
	session_start();
	if(isset($_SESSION['logged'])){
		if($_SESSION['logged']!=1){
			header("Location:login.php");
		}
	}
	$display="";

	//verify if post or a form has been submitted
	if(isset($_POST['submit'])){
	
		
		echo '<center><div class="alert alert-success" role="alert" style="width: 85%;">'."Form has  been submitted"."</div></center>";
		
		
		//this the part where you clean, validate and encrypt,if needed, the data
		$productName	=$_POST['productName'];
		$productDescription =$_POST['productDescription'];
		$price=$_POST['price'];
		$isAvailable="Yes";
		$targetdirectory="../images/";
		$filename=time().$_FILES["photo"]["name"];
		$targetfile=$targetdirectory.$filename;
		
		echo '<center><div class="alert alert-success" role="alert" style="width: 85%;">'.$targetfile."</div></center>";
	
		
		
		if(move_uploaded_file($_FILES["photo"]["tmp_name"],$targetfile)){
		
		
		
		
		try{
			//connect to the database
			require('../controllers/MysqlConnect.php');
				$conn=myConnect();

			echo '<center><div class="alert alert-success" role="alert"  style="width: 85%;">'."Successfully connected to the database"."</div></center>";	
			
		


			//prepare the sql statement
			$strsql="INSERT INTO products(photo, productName, productDescription, isAvailable ,buyPrice)
						VALUES('".$filename."','".$productName."', '".$productDescription."','".$isAvailable."','".$price."')";
			$stmt=$conn->prepare($strsql);
			
			
			//execute statement
			$stmt->execute();

			//check result
			
			
			header('Location:editMenu.php');
			echo '<center><div class="alert alert-success" role="alert"  style="width: 85%;">'."Cake has been Added!"."</div></center>";
			//close the db connection

			$conn=null;


		}catch(PDOException $e){
			print "<br/> Error" .$e->getMessage()."<br/>";
			die();
		}
		
		
			}
		}else
		{
			echo '<center><div class="alert alert-info" role="alert"  style="width: 85%;">'."Please fill up form."."</div></center>";
		}
		
	
?>

<!-- start of content -->
	<div class="container" style="background-color:white; padding:150px; border-radius:10px; margin-bottom: 50px;">
	
	
	<div class="row">
		<div class="col-md-6">
			<form action="" method="POST" enctype="multipart/form-data">
				<div class="form-group">
				<label for="exampleInputEmail1" >Food Name</label>
				<input type="text" class="form-control" name="productName" id="exampleInputEmail1" placeholder="Food Name">
			  </div>
			  <div class="form-group">
				<label for="exampleInputEmail1">Food Photo</label>
				<input type="file" class="form-control" name="photo" id="exampleInputEmail1" placeholder="Email" accept="image/*">
			  </div>
			  
			  <div class="form-group">
				<div class="form-group">
				<label for="exampleInputEmail1" >Product Description</label>
				<textarea class="form-control" rows="6" name="productDescription" placeholder="Product Description"></textarea>
			  </div>
				
		
				<div class="form-group">
				<label for="exampleInputEmail1" >Price</label>
				<input type="text" class="form-control" name="price" id="exampleInputEmail1" placeholder="Price">
			  </div>
				
			
			  </div>

			  <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
			  <button type="reset" class="btn btn-danger">Reset</button>
			</form>
				
		</div>
		<div class="col-md-6">
		</div>
	</div>
		
	</div>

<!-- end of content -->
<?php
	include 'footer.php';
?>

