<?php
include 'header.php';
?>
<?php require('../controllers/MysqlConnect.php');
$conn=myConnect();
?>
<?php
$sql = "SELECT  * FROM products";
$result = mysqli_query($conn,$sql)
or 
die("Error: ".mysqli_error($conn));


?>

<html>
<body>
	<div class='panel panel-primary' style='width:90%; margin:5% auto;'>
	<div class='panel-heading'>
	<h1 style='text-align:center;'>Edit Cakes</h1>
	</div>
	<div class='panel-body'>
	<table id='productTable'>
		<thead>
			<th>Actions</th>
			<th></th>
			<th>Product Name</th>
			<th>Product Description</th>
			<th>Availability</th>
			<th>Price</th>
			</thead>
		</thead>

	<tbody>
	<?php
		while($row = mysqli_fetch_array($result)){ 
		echo "<tr>";
		echo "<td><a href='edit.php?id={$row['productID']}'><button type='button' class='btn btn-success'>Edit</button></a></td>";
		
		echo "<td><a href='deleteCake.php?id={$row['productID']}'><button type='button' onclick='myFunction()' window.header='editMenu.php' 
		class='btn btn-danger'>Delete</button></a></td>";
		echo "<td> $row[2]</td>";
		echo "<td> $row[3]</td>";
		echo "<td> $row[4]</td>";
		echo "<td> $row[5]</td>";
		echo "</tr>";
	 } 
	 ?>



	
	</tbody>
	</table>
	</div>
</body>
</html>
<script src='datatable_files/jquery.min.js'></script>
<script src='datatable_files/datatables.min.js'></script>
<script>
$(document).ready(function(){
	$("#productTable").DataTable();
	});
function myFunction()
{
alert("Cake has been Deleted!!");
}
</script>

<?php
include 'footer.php';
?>
